package com.example.demo;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MainTest {
    private static WebDriver driver;
    private static Main main;

    @Test
    @DisplayName("Tests")
    public void Tests() {
        driver = new ChromeDriver();
        main = new Main(driver);

        main.openGoogleCloudHomePage();
        main.searchForPricingCalculator();
        main.goToPricingCalculatorPage();
        main.clickAddToEstimate();
        main.clickComputeEngine();
        main.acceptCookies();
        main.fillOutForm();
        main.selectOperatingSystem("Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)");
    	main.selectProvisioningModel();
        main.selectMachineType("n1-standard-8");
        main.clickCheckbox();
        main.selectGpuModel("nvidia-tesla-t4"); //other option doesn't have Frankfurt location needed in next step
        main.selectNumberOfGPU("1");
        main.selectSSD("2x375 GB");
        main.selectDatacenterLocation("europe-west3");
        main.commitedUse("1-year");
//		main.clickAddToEstimateButton(); //not needed most probably, making next steps impossible
        main.retrieveTotalEstimatedCost();
        main.clickShareButton();
        main.clickOpenEstimateSummaryButton();

        assertTrue(main.compareCosts(), "Estimated costs are not matching");
    }

	@AfterAll
	public static void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}
}
