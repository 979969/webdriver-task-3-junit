package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

public class Main {
    public double totalEstimatedCost;

    private WebDriver driver;

    public Main(WebDriver driver) {
        this.driver = driver;
    }

    public void openGoogleCloudHomePage() {
        driver.get("https://cloud.google.com/");
    }

    public void searchForPricingCalculator() {
        WebElement searchIcon = driver.findElement(By.cssSelector("div.ND91id"));
        searchIcon.click();

        WebElement searchField = driver.findElement(By.name("q"));
        searchField.sendKeys("Google Cloud Platform Pricing Calculator");
        searchField.submit();
    }

    public void goToPricingCalculatorPage() {
        WebElement pricingCalculatorLink = driver.findElement(By.linkText("Google Cloud Pricing Calculator"));
        pricingCalculatorLink.click();
    }

    public void clickAddToEstimate() {
        WebElement addToEstimateButton = driver.findElement(By.xpath("//span[text()='Add to estimate']"));
        addToEstimateButton.click();
    }

    public void clickComputeEngine() {
        WebElement computeEngineButton = driver.findElement(By.xpath("//h2[text()='Compute Engine']"));
        computeEngineButton.click();
    }

    public void acceptCookies() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        try {
            WebElement acceptCookiesButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.glue-cookie-notification-bar__accept")));
            acceptCookiesButton.click();
        } catch (Exception e) {
            System.out.println("Cookie notification bar not found or already accepted.");
        }
    }

    public void fillOutForm() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement addButton = driver.findElement(By.xpath("//button[@aria-label='Increment']"));

        addButton.click();
        addButton.click();
        addButton.click();
    }

    public void selectOperatingSystem(String operatingSystem) {
        WebElement operatingSystemDropdown = driver.findElement(By.xpath("//div[@data-field-type='106']"));
        operatingSystemDropdown.click();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//li[@data-value='" + operatingSystem + "']")));

        WebElement option = driver.findElement(By.xpath("//span[contains(text(), '" + operatingSystem + "')]"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", option);
    }


    public void selectProvisioningModel() {
        WebElement provisioningModelInput = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[9]/div/div/div[2]/div/div/div[1]/label")));
        provisioningModelInput.click();
    }


    public void selectMachineType(String machineType) {
        WebElement dropdownIcon = driver.findElement(By.xpath("//*[@id='ow4']/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[11]/div/div/div[2]/div/div[1]/div[3]/div/div"));
        dropdownIcon.click();

        WebElement machineTypeOption = driver.findElement(By.xpath("//li[@data-value='" + machineType + "']"));
        machineTypeOption.click();
    }

    public void clickCheckbox() {
        WebElement checkbox = driver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[21]/div/div/div[1]/div/div/span/div/button/div"));
        checkbox.click();
    }

    public void selectGpuModel(String gpuModel) {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[23]/div/div[1]/div/div/div"));
        dropdown.click();

        WebElement option = driver.findElement(By.xpath("//li[@data-value='" + gpuModel + "']"));
        option.click();
    }

    public void selectNumberOfGPU(String gpuNumber) {
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[24]/div"));
        dropdown.click();

        WebElement option = driver.findElement(By.xpath("//li[@data-value='" + gpuNumber + "']"));
        option.click();
    }

    public void selectSSD(String ssdType) {
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[27]/div"));
        dropdown.click();

        WebElement option = driver.findElement(By.xpath("//li[contains(.,'" + ssdType + "')]"));
        option.click();
    }

    public void selectDatacenterLocation(String location) {
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[29]/div"));
        dropdown.click();

        WebElement option = driver.findElement(By.xpath("//li[contains(.,'" + location + "')]"));
        option.click();
    }

    public void commitedUse(String years) {
        WebElement inputField = driver.findElement(By.xpath("//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[31]/div/div/div[2]/div/div/div[2]"));
        inputField.click();
    }

//not needed most probably, making next steps impossible, leaving it just in case (7. Click 'Add to Estimate'.)
//	public void clickAddToEstimateButton() {
//		WebElement button = driver.findElement(By.xpath("//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[2]/div[1]/div/div[2]/div"));
//		button.click();
//	}

    //1st cost
    public void retrieveTotalEstimatedCost() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement totalCostElement = driver.findElement(By.cssSelector(".gt0C8e.MyvX5d.D0aEmf"));
        String totalCostText = totalCostElement.getText();
        String costValue = totalCostText.replaceAll("[^0-9.]", "");
        totalEstimatedCost = Double.parseDouble(costValue);
    }


    public void clickShareButton() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement shareButton = driver.findElement(By.cssSelector("button[aria-label='Open Share Estimate dialog']"));
        shareButton.click();
    }

    public void clickOpenEstimateSummaryButton() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement openEstimateSummaryButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(), 'Open estimate summary')]")));

        openEstimateSummaryButton.click();
        Set<String> handles = driver.getWindowHandles();

        for (String handle : handles) {
            if (!handle.equals(driver.getWindowHandle())) {
                driver.switchTo().window(handle);
                break;
            }
        }
    }

    //2nd cost
    public double extractNewEstimatedCost() {
        WebElement newCostElement = driver.findElement(By.xpath("//*[@id=\"yDmH0d\"]/c-wiz[1]/div/div/div/div/div[1]/div/div[1]/div[1]/h4"));
        String newCostText = newCostElement.getText().replaceAll("[^0-9.]", "").replaceAll(",", ""); // Extract numerical value and remove commas
        return Double.parseDouble(newCostText);
    }

    public boolean compareCosts() {
        double newCostText = extractNewEstimatedCost();
        return totalEstimatedCost == newCostText;
    }

}




